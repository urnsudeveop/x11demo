unit uArrays;
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
type

UAssociativeArray = Class
Elements          : array of string                     ;
uIndex            : TStrings                            ;
arrofarr          : array of array of string;
public

constructor      Create                                 ;
destructor       Destroy                     ; override ;
function         get(index:string):string               ;
procedure        add(index,val:string)                  ;
function         key_on_array(index:string):boolean         ;
function         value_on_array(value:string):boolean         ;
end;

type UAssocException = Class(Exception)                 ;

implementation

constructor UAssociativeArray.Create                    ;
begin
uIndex := TStringList.Create                            ;
end;

destructor UAssociativeArray.Destroy                    ;
begin
FreeAndNil(uIndex)                                      ;
end;

function UAssociativeArray.get(index: string): string;
var
  i: integer                                            ;
begin
  i := uIndex.IndexOf(index)                            ;
  if i > -1 then
  begin
  result := Elements[i]                                 ;
  end
  else
  begin
  raise UAssocException.Create('not found index '+index);
  end;
end;

function UAssociativeArray.key_on_array(index:string):boolean;
begin
  if uIndex.IndexOf(index) < 0 then
  begin
       result:=false;
  end
  else begin
       result:=true;
  end;

end;

function UAssociativeArray.value_on_array(value:string):boolean;
var
  i:integer;
begin
   for i := Low(Elements) to High(Elements) do
    begin
           writeln(Elements[i]);
    if Elements[i] = value then
    begin
      Result := True;
      Exit;
    end;
  Result := False;
    end;

end;

procedure UAssociativeArray.add(index,val: string)   ;
var
  i,len: integer;
begin
  i := uIndex.IndexOf(index)                                           ;
  if i = -1 then
  begin
    len                                       := length(Elements)      ;
    SetLength(Elements,len+1)                                          ;
    Elements[len]                             := val                   ;
    uIndex.Add(index)                                                  ;
    i                                         := uIndex.IndexOf(index) ;
    Elements[i]                               := val                   ;
  end else
  begin
    Elements[i]                               := val                   ;
  end;
end;
end.

