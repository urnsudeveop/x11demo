unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Grids,
  ExtCtrls, PairSplitter, uSearch, Embed, LazUTF8, AnchorDockPanel, strutils,
  types;

type

  { TForm1 }

  TForm1 = class(TForm)
    AnchorDockPanel1: TAnchorDockPanel;
    AnchorDockPanel2: TAnchorDockPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button5: TButton;
    Edit1: TEdit;
    EmbedControl1: TEmbedControl;
    Grid: TStringGrid;
    PairSplitter1: TPairSplitter;
    PairSplitterSide1: TPairSplitterSide;
    PairSplitterSide2: TPairSplitterSide;
    Panel1: TPanel;
    Panel2: TPanel;


    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GridClick(Sender: TObject);
    procedure GridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure PairSplitterSide1Resize(Sender: TObject);
  private

  public
      search:TxSearch                                                     ;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var

PID,WID,WTITLE,WCLASS:String                                               ;
x,rc:integer                                                           ;
elpid:String;
begin


grid:= EmbedControl1.WindowsList();
grid.OnSelectCell:=@GridSelectCell;
grid.Align:=alClient;
grid.ColWidths[0]:=100;
grid.ColWidths[1]:=100;
grid.ColWidths[2]:=500;
grid.ColWidths[3]:=500;
grid.Parent:=Panel2;
grid.Visible:=true;




end;

procedure TForm1.Button2Click(Sender: TObject);
var
elpid:string;
winid:string;
index:integer;
begin
  EmbedControl1.start;
  elpid:=EmbedControl1.elpid.ToString;
 self.Button1Click(self);

 winid:=search.GetWIDfromPID(EmbedControl1.elpid);
 search.FocusWindow(search.uWindowid(winid.ToLower));
end;

procedure TForm1.Button3Click(Sender: TObject);
var
elid,emeddedcontainer,hexstr:String;
container:PtrUInt;
begin
                            hexstr:=Edit1.text;
                            hexstr:=hexstr.Replace('0x','');
  container:=EmbedControl1.GetHWND();

  search.FocusWindow(search.uWindowid(hexstr.ToLower));
  end;

procedure TForm1.Button4Click(Sender: TObject);

begin


end;

procedure TForm1.Button5Click(Sender: TObject);
begin
EmbedControl1.embedApp('epiphany');
grid:= EmbedControl1.WindowsList();
grid.OnSelectCell:=@GridSelectCell;
grid.Align:=alClient;
grid.ColWidths[0]:=100;
grid.ColWidths[1]:=100;
grid.ColWidths[2]:=500;
grid.ColWidths[3]:=500;
grid.Parent:=Panel2;
grid.Visible:=true;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
            search:=TxSearch.Create                                 ;
            //  search.init()                                           ;
end;

procedure TForm1.GridClick(Sender: TObject);
begin

end;

procedure TForm1.GridSelectCell(Sender: TObject; aCol, aRow: Integer;
  var CanSelect: Boolean);
var
splite:TStringDynArray;
sp:String;
begin             try
                      sp:=grid.Rows[aRow].CommaText   ;
            splite:=splitstring(sp,',');
                       sp:=splite[1];
                   Edit1.Text:= sp;

except
;
end;
end;

procedure TForm1.PairSplitterSide1Resize(Sender: TObject);
begin
            EmbedControl1.Align:=alClient;
end;

end.

