{
Coder: Nocturnus / urnusdev@zohomail.com
XWINDOW ID AND PID  ( LINUX )
Obtener listado PID e ID de la XWINDOW


Debian 10
Free Pascal Compiler version 3.2.0+dfsg-8 [2020/08/22] for x86_64

}

program findXIDs;
uses
   x,
  crt,
  xlib,
  xatom,
  unix,
  xutil,
  ctypes,
  sysutils
  ;

procedure chl(Display:PDisplay; childw:TWindow;atomPID:TAtom);
var
j,xnwindows:integer;
xmyw,xparentw,uwin:TWindow;
xchildw:PPWindow;
prop:ppcuchar;
bytes_after_ret: pculong;
type_ret: TAtom;
format_ret: integer;
nitems_ret: pculong;
myprop: PCardinal;
ELPID:TWINDOW;

windowattr: TXWindowAttributes;
WindowName:PChar;
begin
  XQueryTree(Display, childw, @xmyw, @xparentw, @xchildw, @xnwindows);
     for j := 0 to xnwindows - 1 do
  begin


           uwin := TWindow(xchildw[j]);


           XGetWindowProperty(
     Display,
     TWindow(xchildw[j]),
     atomPID,
     0,
     1,
     False,
     XA_CARDINAL,
     @type_ret,
     @format_ret,
     @nitems_ret,
     @bytes_after_ret,
     @prop
     );


     if prop > PPcuchar(0) then
     begin
     myprop := PCardinal(prop);
     ELPID := TWindow(myprop^);
     XGetWindowAttributes(Display, TWindow(xchildw[j]), @windowattr) ;

    if XFetchName(Display, TWindow(xchildw[j]), @WindowName) <> 1 then WindowName := 'Undefined';
    writeln(' * PID:'+ELPID.ToString+' WindowID: 0x'+uwin.ToHexString(8)+' WName: '+WindowName);


        end;
   end;

end;

function search():Boolean;
var
  Display:PDisplay;
  atomPID:TAtom;
  type_ret: TAtom;
  format_ret: integer;
  nitems_ret: pculong;
  bytes_after_ret: pculong;
  rootw: TWindow;
  myw: TWindow;
  parentw: TWindow;
  childw: PWindow;
  //windowattr: TXWindowAttributes;
  //n, c: PChar;
  //wname, cname: string;
  nwindows: integer;
  i: integer;
  prop:ppcuchar;
  //myprop: PCardinal;
  //ELPID:TWindow;
begin
             result := false;
                   Display:= XOpenDisplay(nil);



                    rootw := RootWindow(Display, DefaultScreen(Display));

             atomPID:=XInternAtom(Display,'_NET_WM_PID',true);
             XQueryTree(Display, rootw, @myw, @parentw, @childw, @nwindows);
        for i := 0 to nwindows - 1 do
        begin

  XGetWindowProperty(
     Display,
     childw[i],
     atomPID,
     0,
     1,
     False,
     XA_CARDINAL,
     @type_ret,
     @format_ret,
     @nitems_ret,
     @bytes_after_ret,
     @prop
     );



   chl(Display, childw[i],atomPID);

   { if XGetWindowAttributes(Display, childw[i], @windowattr) <> 1 then
      continue;
     if windowattr.override_redirect = 1 then
      continue;

    if XFetchName(Display, childw[i], @n) = 1 then
      wname := n
    else wname := 'n/a';
    if XGetClassHint(Display, childw[i], @c) = 1 then
      cname := c
    else cname := 'n/a';

   if prop > PPcuchar(0) then
     begin

       myprop := PCardinal(prop);
       ELPID := TWindow(myprop^);
       writeln(inttostr(i) + ' Name: ' + wname + ' Class: ' + cname + ' PID:'+ELPID.ToString);

     end; }
  end;
  XFree(childw);
  XCloseDisplay(Display);

  result :=false;
end;
begin
                textcolor(white);
       search();

end.

