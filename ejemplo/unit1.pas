unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Embbeder,
  x
  ,xlib
  ,xatom,
  unix,
  xutil,
  ctypes
  ;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    EmbedderControl1: TEmbedderControl;
    procedure Button1Click(Sender: TObject);
    procedure EmbedderControl1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
            function search():Boolean;
            procedure chl(Display:PDisplay; childw:TWindow; atomPID:TAtom);

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormActivate(Sender: TObject);
begin
           //    EmbedderControl1.start;
                Self.search();
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   //  Self.search();
end;

procedure TForm1.EmbedderControl1Click(Sender: TObject);
begin
  showmessage('click');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   Self.search();
end;

procedure TForm1.chl(Display:PDisplay; childw:TWindow;atomPID:TAtom);
var
j,xnwindows:integer;
xmyw,xparentw,uwin:TWindow;
xchildw:PPWindow;
prop:ppcuchar;
bytes_after_ret: pculong;
type_ret: TAtom;
format_ret: integer;
nitems_ret: pculong;
myprop: PCardinal;
ELPID:TWINDOW;

 // windowattr: TXWindowAttributes;
 // n,c:PChar;
 // wname,cname:String;
begin
  XQueryTree(Display, childw, @xmyw, @xparentw, @xchildw, @xnwindows);
     for j := 0 to xnwindows - 1 do
  begin


           uwin := TWindow(xchildw[j]);


           XGetWindowProperty(
     Display,
     TWindow(xchildw[j]),
     atomPID,
     0,
     1,
     False,
     XA_CARDINAL,
     @type_ret,
     @format_ret,
     @nitems_ret,
     @bytes_after_ret,
     @prop
     );


     if prop > PPcuchar(0) then
     begin
     myprop := PCardinal(prop);
     ELPID := TWindow(myprop^);
    // writeln('Parent: '+xparentw.ToHexString());
     writeln(' * PID:'+ELPID.ToString+' WindowID: 0x'+uwin.ToHexString(8));

    {  if XGetWindowAttributes(Display, TWindow(xchildw[j]), @windowattr) <> 1 then
        begin


          if XFetchName(Display, TWindow(xchildw[j]), @n) = 1 then
      wname := n
    else wname := 'n/a';
    if XGetClassHint(Display, TWindow(xchildw[j]), @c) = 1 then
      cname := c
    else cname := 'n/a';
    writeln(inttostr(j) + ' Name: ' + wname + ' Class: ' + cname + ' PID:'+ELPID.ToString);


        myprop := PCardinal(prop);
        ELPID := TWindow(myprop^);
       writeln(inttostr(j) + ' ---------- PID:'+ELPID.ToString+' Name:'+wname+' Class:'+cname+' WID:'+uwin.ToHexString(8));
       end;
              }
        end;
   end;

end;

function TForm1.search():Boolean;
var
  Display:PDisplay;
  atomPID:TAtom;
  type_ret: TAtom;
  format_ret: integer;
  nitems_ret: pculong;
  bytes_after_ret: pculong;
  rootw: TWindow;
 // DefaultWindow:TWindow;
  myw: TWindow;
 // xmyw: TWindow;
//  uwin:TWindow;
  parentw: TWindow;
  //xparentw: TWindow;
  childw: PWindow;
  //xchildw: PWindow;
  windowattr: TXWindowAttributes;
  n, c: PChar;
  wname, cname: string;
  nwindows: integer;
  //xnwindows: integer;
  i: integer;
  prop:ppcuchar;// PAtomArray;
 //  xprop:ppcuchar;
 //  varian:pcuchar;
   myprop: PCardinal;
 //  xmyprop: PCardinal;
   ELPID:TWindow;
//   xELPID:TWindow;
begin
             result := false;
                   Display:= XOpenDisplay(nil);



                    rootw := RootWindow(Display, DefaultScreen(Display));

           // DefaultWindow:=        XDefaultRootWindow(Display);

             atomPID:=XInternAtom(Display,'_NET_WM_PID',true);
             XQueryTree(Display, rootw, @myw, @parentw, @childw, @nwindows);
        for i := 0 to nwindows - 1 do
        begin


           //uwin := TWindow(childw[i]);
  XGetWindowProperty(
     Display,
     childw[i],
     atomPID,
     0,
     1,
     False,
     XA_CARDINAL,
     @type_ret,
     @format_ret,
     @nitems_ret,
     @bytes_after_ret,
     @prop
     );



   Form1.chl(Display, childw[i],atomPID);
 {
XQueryTree(Display, childw[i], @xmyw, @xparentw, @xchildw, @xnwindows);
                 for j := 0 to xnwindows - 1 do
                   begin
                   XGetWindowProperty(
     Display,
     xchildw[j],
     atomPID,
     0,
     1,
     False,
     XA_CARDINAL,
     @type_ret,
     @format_ret,
     @nitems_ret,
     @bytes_after_ret,
     @xprop
     );

                   if xprop > PPcuchar(0) then
     begin

        xmyprop := PCardinal(xprop);
        xELPID := TWindow(xmyprop^);
       writeln(inttostr(i) + ' ---------- PID:'+xELPID.ToString);
   end;
                   end;


          }

    if XGetWindowAttributes(Display, childw[i], @windowattr) <> 1 then
      continue;
{    if windowattr.override_redirect = 1 then
      continue;
                }
    if XFetchName(Display, childw[i], @n) = 1 then
      wname := n
    else wname := 'n/a';
    if XGetClassHint(Display, childw[i], @c) = 1 then
      cname := c
    else cname := 'n/a';

   if prop > PPcuchar(0) then
     begin

       myprop := PCardinal(prop);
       ELPID := TWindow(myprop^);
       writeln(inttostr(i) + ' Name: ' + wname + ' Class: ' + cname + ' PID:'+ELPID.ToString);

     end;
  end;
  XFree(childw);
  XCloseDisplay(Display);

  result :=false;
end;

end.

