unit Embbeder;

{$mode objfpc}{$H+}

{$ifdef Linux}
 {$ifndef LCLgtk2}
 {$error this unit only supports LCL under gtk2}
 {$endif}
{$endif}

interface

uses
  Classes, SysUtils, Controls, WSLCLClasses, LCLProc, LCLType, InterfaceBase,
  LResources, LMessages, Graphics, ExtCtrls, FileUtil, Process, UTF8Process,
  LazFileUtils,Grids
  {$ifdef Linux}
  , gtk2int, gtk2, glib2, gdk2x, Gtk2WSControls, GTK2Proc, Gtk2Def,xlib
  {$endif}
  ;

type
  { TCustomEmbedder }

  TCustomEmbedder = class(TWinControl)
  private

    FFilename: string;

    FStartParam:string;

    FXTermPath: string;
   // FPaused: boolean;

    XPROCESS:TProcessUTF8;
    uXTermProcess: TProcessUTF8;
    FocusXtermProcess: TProcessUTF8;
    FTimer: TTimer;

    FCanvas: TCanvas;
    FOutList: TStringList;
  //  xSearch: TxSearch;
    procedure SetFilename(const AValue: string);

    procedure SetXTermPath(const AValue: string);

    procedure SetStartParam(const AValue: string);

  protected
    procedure WMPaint(var Message: TLMPaint); message LM_PAINT;
    procedure WMSize(var Message: TLMSize); message LM_SIZE;


  public

    constructor Create(TheOwner: TComponent); override;
    procedure AfterConstruction(); override;
    destructor Destroy; override;
    procedure SendXTermCommand(Cmd: string); // see: XTerm -input cmdlist and http://www.XTermhq.hu/DOCS/tech/slave.txt
    function Running: boolean;
    procedure start;
    procedure Focusin(sender:TObject);
    procedure Stop;

    procedure Invalidate; override;
    procedure EraseBackground(DC: HDC); override;
    function GetHWND():PtrUInt;
  public

    cs: TRTLCriticalSection;
    elpid:LongInt;
     CurWindowID: PtrUInt;
     curwinid:String;
      function lchild():boolean;
    function FindXTermPath : Boolean;

    procedure GrabImage;
    procedure ListenClick();
    procedure embed(application:String;params:TProcessStrings);

    property Filename: string read FFilename write SetFilename;
    property StartParam: string read FStartParam write SetStartParam;
    property XTermPath: string read FXTermPath write SetXTermPath;
    property PlayerProcess: TProcessUTF8 read uXTermProcess;  deprecated;

  published


  end;

  TEmbedderControl = class(TCustomEmbedder)
    private

       FInfo: Pointer;
           FOnClick: TNotifyEvent;

  published
       procedure DoOnClick(Sender: TObject);

    property Align;
    property Anchors;
    property BorderSpacing;
    property Enabled;
    property Filename;

    property OnChangeBounds;
    property OnConstrainedResize;
    property OnResize;
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    //property OnClick;
    property OnMouseUp;
    property OnMouseDown;
    property OnMouseWheel;
    property Visible;


  public
       constructor Create(TheOwner: TComponent); override;

  end;

  { TWSEmbedder }

  {$ifdef Linux}
  TWSEmbedder = class(TGtk2WSWinControl)
  published
    class function CreateHandle(const AWinControl: TWinControl;
                                const AParams: TCreateParams): HWND; override;
    class procedure DestroyHandle(const AWinControl: TWinControl); override;
  end;
  {$endif}

Const
  ON_PLAYING_INTERVAL = 500 / (24*60*60*1000);

procedure Register;

implementation

Uses
  Forms,SyncObjs;

procedure Register;
begin
  RegisterComponents('Embbeder',[TEmbedderControl]);
end;

// returns the value from "ANS_PropertyName=Value" strings
function ExtractAfter(AInput, AIdentifier: string): string; inline;
begin
  AInput := Lowercase(AInput);
  AIdentifier := Lowercase(AIdentifier);

  Result := Copy(AInput, Length(AIdentifier) + 1, Length(AInput) - Length(AIdentifier));
end;

{ TCustomEmbedder }



constructor TEmbedderControl.Create(TheOwner: TComponent);
begin
      inherited;
    //    CurWindowID := GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window);

  end;

       procedure TEmbedderControl.DoOnClick(Sender: TObject);
begin

  inherited Click;
  if Assigned(FOnClick) then
    FOnClick(Self);
end;

procedure TCustomEmbedder.embed(application:String;params:TProcessStrings);
var
  winid:String;
  phandle,ha:String;

begin
//  xSearch:=TxSearch.Create;
          XPROCESS := TProcessUTF8.Create(Self);
  XPROCESS.Options := XPROCESS.Options + [poUsePipes];    //, poNoConsole
   XPROCESS.Executable:=application;
  winid:=GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window).ToHexString(8);
  winid:='0x'+winid;
  if assigned(params) then XPROCESS.Parameters:=params;
  XPROCESS.Execute;
  self.elpid:=XPROCESS.ProcessID;
  phandle:=XPROCESS.ProcessHandle.ToHexString(8);
  ha:=XPROCESS.Handle.ToHexString(8);
  //sleep(1000);
  //XPROCESS.Suspend;

 // XReparentWindow(  XOpenDisplay(nil),
   //XPROCESS.Resume;

end;

procedure TCustomEmbedder.WMPaint(var Message: TLMPaint);
begin
  Include(FControlState, csCustomPaint);
  inherited WMPaint(Message);
  if (csDesigning in ComponentState) and (FCanvas<>nil) then begin
    with FCanvas do begin
      if Message.DC <> 0 then
        Handle := Message.DC;
      Brush.Color:=clBlack;
      Pen.Color:=$00ADBF00;
      Rectangle(0,0,Self.Width-1,Self.Height-1);
      MoveTo(0,0);
   //   LineTo(Self.Width,Self.Height);
      MoveTo(0,Self.Height);
    //  LineTo(round(Self.height/int(2)),round(Self.height/int(2)));
      if Message.DC <> 0 then
        Handle := 0;
    end;
  end;

   FCanvas.TextOut(0,0,'EMBBEDER');

  writeln('TCustomEmbedder.WMPaint');
  writeln(Message.Msg.ToString);

  Exclude(FControlState, csCustomPaint);
end;

procedure TCustomEmbedder.WMSize(var Message: TLMSize);
begin
  if (Message.SizeType and Size_SourceIsInterface)>0 then
    DoOnResize;
end;

procedure TCustomEmbedder.SetStartParam(const AValue: string);
begin
  if FStartParam=AValue then exit;
  FStartParam:=AValue;
end;

procedure TCustomEmbedder.SetFilename(const AValue: string);
  // Copied from win\process.inc
  // XTerm uses identical params under linux, so this is safe
  Function MaybeQuoteIfNotQuoted(Const S : String) : String;
  begin
    If (Pos(' ',S)<>0) and (pos('"',S)=0) then
      Result:='"'+S+'"'
    else
       Result:=S;
  end;
begin
  if FFilename=AValue then exit;
  FFilename:=AValue;
  if Running then
    SendXTermCommand('loadfile '+MaybeQuoteIfNotQuoted(Filename));
end;


procedure TCustomEmbedder.SetXTermPath(const AValue: string);
begin
  if FXTermPath=AValue then exit;
  FXTermPath:=AValue;
end;

constructor TCustomEmbedder.Create(TheOwner: TComponent);
var
  flag:boolean;


begin

  inherited Create(TheOwner);
                        self.SetFocus;
                          flag:=     self.CanFocus;
            flag:=  self.CanSetFocus;
  ControlStyle:=ControlStyle-[csSetCaption];
  FCanvas := TControlCanvas.Create;
  TControlCanvas(FCanvas).Control := Self;
  SetInitialBounds(0, 0, 160, 90);

  FOutlist := TStringList.Create;

  FXTermPath := 'xterm' + GetExeExt;
end;

procedure TCustomEmbedder.AfterConstruction();
var
  th:THandle;
begin
   //       th:=self.Handle;;

end;

destructor TCustomEmbedder.Destroy;
begin
  Stop;
  FreeAndNil(FCanvas);
  FreeAndNil(FTimer);
  FreeAndNil(FOutList);
  inherited Destroy;
end;

procedure TCustomEmbedder.SendXTermCommand(Cmd: string);
begin
  if Cmd='' then exit;
  if not Running then exit;

  if Pos('paus', Lowercase(Cmd)) <> 1 then
    Cmd := 'pausing_keep_force ' + Cmd;
  if Cmd[length(Cmd)] <> LineEnding then
    Cmd := Cmd + LineEnding;

  uXTermProcess.Input.Write(Cmd[1], length(Cmd));
end;

function TCustomEmbedder.Running: boolean;
begin
  Result:=(uXTermProcess<>nil) and uXTermProcess.Running;
end;

function TCustomEmbedder.FindXTermPath: Boolean;
var
  ExePath: string;
  XTermExe: String;
begin
  result := FileExistsUTF8(FXTermPath);

  If not result then
  begin
    XTermExe:='XTerm'+GetExeExt;
    if FXTermPath='' then
      FXTermPath:=XTermExe;
    ExePath:=FXTermPath;
    // Is XTerm installed in the environment path?
    if not FilenameIsAbsolute(ExePath) then
      ExePath:=FindDefaultExecutablePath(ExePath);
    // is XTerm in a folder under the application folder?
    if Not FileExistsUTF8(ExePath) then
      ExePath := IncludeTrailingBackSlash(ExtractFileDir(Application.ExeName))+
        IncludeTrailingBackslash('XTerm') + XTermExe;
    // did we find it?
    if FileExistsUTF8(ExePath) then
    begin
      FXTermPath:=ExePath;
      result := true;
    end;
  end;
end;

procedure TCustomEmbedder.GrabImage;
begin
  if Running then
    SendXTermCommand('screenshot 0')
end;
         procedure TCustomEmbedder.Focusin(Sender:TObject);
    begin
       writeln('focusin');
    end;

function uclicked(widget:PGtkWidget;{%H-}data: gPointer):GBoolean; cdecl;
begin


           DebugLn(['uclicked ',dbgs(widget)]);
           Result:=true;
  end;

function TCustomEmbedder.lchild():boolean;
var
  widget,elchild:PGtkWidget;
   container:PGtkContainer;
   chl:PGList;
   tgo:TGObject;
begin

              widget:=PGtkWidget(PtrUInt(Handle));
              container:= PGtkContainer(Widget);
              chl := gtk_container_get_children(PGtkContainer(Widget));
              gtk2.gtk_window_set_focus(PGtkWindow(PtrUInt(Handle)),widget);
              elchild:=PGtkWidget(chl^.data);
              g_signal_connect(GPointer(elchild), 'clicked', g_callback(@uclicked),NIL);

end;

function TCustomEmbedder.GetHWND():PtrUInt;
begin
     CurWindowID := GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window);
        result:=CurWindowID;
end;

procedure TCustomEmbedder.start;
var
  //CurWindowID: PtrUInt;
//  slStartParams : TStringList;
  winid:String;
  xtermPid:Longint;

  widget,elchild:PGtkWidget;
  zlistchild:PGList;
  container:PGtkContainer;
  st:String;
  runneable:Boolean;
          sl:TStringList;
begin
 InitCriticalSection(cs);

  EnterCriticalSection(cs);
  try
  if (csDesigning in ComponentState) then exit;
  {
  if Running and Paused then begin
    Paused:=false;
    exit;
  end;    }
        {
  if Playing then begin
    if FRate<>1 Then
      Rate := 1;
    exit;
  end;      }

  {$IFDEF Linux}
  if (not HandleAllocated) then exit;
  DebugLn(['TCustomEmbedder.Play ']);
  {$endif}

  if uXTermProcess<>nil then
    FreeAndNil(uXTermProcess);
//    raise Exception.Create('TCustomEmbedder.Play uXTermProcess still exists');

  if not FindXTermPath then
    raise Exception.Create(XTermPath+' not found');

  {$IFDEF Linux}
    CurWindowID := GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window);



  {$else}
    CurWindowID := Handle;
  {$ENDIF}

  uXTermProcess := TProcessUTF8.Create(Self);
  uXTermProcess.Options := uXTermProcess.Options + [poUsePipes];    //, poNoConsole

  // -really-quiet       : DONT USE: causes the video player to not connect to -wid.  Odd...
  // -noconfig all       : stop XTerm from reading commands from a text file
  // -zoom -fs           : Unsure:  Only perceptible difference is background drawn black not green
  // -vo direct3d        : uses Direct3D renderer (recommended under windows)
  // -vo gl_nosw         : uses OpenGL no software renderer
  uXTermProcess.Executable:=FXTermPath;



                      winid:=GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window).ToHexString(8);
                      winid:='0x'+winid;
                      curwinid:=CurWindowID.ToHexString(8);
uXTermProcess.Parameters.Add('-into');
uXTermProcess.Parameters.Add(winid);
uXTermProcess.Parameters.Add('-fg');
uXTermProcess.Parameters.Add('darkorange3');
uXTermProcess.Parameters.Add('-bg');
uXTermProcess.Parameters.Add('black');
uXTermProcess.Parameters.Add('-geometry');
uXTermProcess.Parameters.Add('190x20');

  DebugLn(['TCustomEmbedder.start ', uXTermProcess.Parameters.DelimitedText]);

  // Normally I'd be careful to only use FOutList in the
  // Timer event, but here I'm confident the timer isn't running...
 { if assigned(FOnFeedback) then
  begin
    FOutlist.Clear;
    FOutlist.Add(uXTermProcess.Executable + ' ' + uXTermProcess.Parameters.DelimitedText);
    FOutlist.Add('');
    FonFeedback(Self, FOutlist);
  end;   }

  // Populate defaults
 // InitialiseInfo;

  uXTermProcess.Execute;

 // widget:=PGtkWidget(PtrUInt(uXTermProcess.Handle));
  //       CurWindowID := GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(uXTermProcess.Handle))^.window);

    xtermPid :=   uXTermProcess.ProcessID;
    elpid := xtermPid;
    writeln('-------> '+xtermPid.ToString)     ;
    DebugLn ('-------> '+xtermPid.ToString);
    DebugLn(winid);
    writeln(winid);

     {
    FocusXtermProcess := TProcessUTF8.Create(Self);
  FocusXtermProcess.Options := FocusXtermProcess.Options + [poUsePipes,poWaitOnExit]; //,poWaitOnExit
   FocusXtermProcess.Executable:='/usr/bin/xdotool';
  //         FocusXtermProcess.CommandLine:='/usr/bin/xdotool search --pid '+xtermPid.ToString+' > outid';
   FocusXtermProcess.Parameters.Add('search');
    FocusXtermProcess.Parameters.Add('--pid');
    FocusXtermProcess.Parameters.Add(xtermPid.ToString);
  {    FocusXtermProcess.Active:=true;                    }
    FocusXtermProcess.Execute;

        }


                      {    sl:=TStringList.Create;
            sl.LoadFromFile('./outid');

            writeln(' --> '+sl.GetText);  }
 // runneable:=uXTermProcess.Running;
  // Start the timer that handles feedback from XTerm
  DebugLn(uXTermProcess.Parameters.ToString);

 // g_signal_connect(GPointer(PGtkWidget(PtrUInt(Handle))), 'clicked', g_callback(@uclicked),NIL);


     finally

       end;
      CurWindowID := GDK_WINDOW_XWINDOW({%H-}PGtkWidget(PtrUInt(Handle))^.window);

  widget:=PGtkWidget(PtrUInt(Handle));
  container:= PGtkContainer( widget);
  container^.focus_child:=widget;
 zlistChild:=    gtk_container_get_children(container);
end;

procedure TCustomEmbedder.Stop;
begin
  if uXTermProcess = nil then
    exit;

  DebugLn(Format('ExitStatus=%d', [uXTermProcess.ExitStatus]));
 // FPaused := False;
//  FDuration := -1;
 // FTimer.Enabled := False;

  SendXTermCommand('exit');
    uXTermProcess.Terminate(1);
  FreeAndNil(uXTermProcess);

  {if Assigned(FOnStop) then
    FOnStop(Self);
    }
  // repaint the control
  Refresh;
end;
   {
function TCustomEmbedder.Playing: boolean;
begin
  Result := Running and (not Paused);
end;        }

procedure TCustomEmbedder.Invalidate;
begin
  if csCustomPaint in FControlState then exit;
  inherited Invalidate;
end;

procedure TCustomEmbedder.ListenClick();
begin

end;

procedure TCustomEmbedder.EraseBackground(DC: HDC);
begin
  if (FCanvas <> nil) then
    with FCanvas do
    begin
      if DC <> 0 then
        Handle := DC;
      Brush.Color := clLtGray;
      Rectangle(0, 0, Self.Width, Self.Height);
      if DC <> 0 then
        Handle := 0;
    end;
end;

{$ifdef Linux}
function XTermWidgetDestroyCB(Widget: PGtkWidget; {%H-}data: gPointer): GBoolean; cdecl;
begin
  FreeWidgetInfo(Widget); // created in TWSEmbedder.CreateHandle
  Result:=false;
end;


{ TWSEmbedder }

function clickonXterm(gwidget:PGtkWidget;p:gpointer):boolean;
begin
                   writeln('clicked');
  result := true;
  end;

class function TWSEmbedder.CreateHandle(const AWinControl: TWinControl;
  const AParams: TCreateParams): HWND;
var
  NewWidget: PGtkWidget;
  WidgetInfo: PWidgetInfo;
  Allocation: TGTKAllocation;
  eventbox:PGtkEventBox;

  Info: PWidgetInfo;
 begin

  if csDesigning in AWinControl.ComponentState then
    Result:=inherited CreateHandle(AWinControl,AParams)
  else begin
    NewWidget:=gtk_event_box_new;


 //   Info.LCLObject := AWinControl;
 // Info.Style := AParams.Style;
 // Info.ExStyle := AParams.ExStyle;

    WidgetInfo := GetWidgetInfo(NewWidget,true); // destroyed in XTermWidgetDestroyCB
    WidgetInfo^.LCLObject := AWinControl;
    WidgetInfo^.Style := AParams.Style;
    WidgetInfo^.ExStyle := AParams.ExStyle;
    WidgetInfo^.WndProc := {%H-}PtrUInt(AParams.WindowClass.lpfnWndProc);
    TEmbedderControl(AWinControl).FInfo := WidgetInfo;

     gtk_widget_set_events( NewWidget ,4);
      gtk_signal_connect (GTK_OBJECT(NewWidget), 'button_press_event',
                        GTK_SIGNAL_FUNC (@clickonXterm), NULL);
           //    gtk2.gtk_window_set_keep_above(PGtkWindow(NewWidget^.window),true);
    // set allocation
    Allocation.X := AParams.X;
    Allocation.Y := AParams.Y;
    Allocation.Width := AParams.Width;
    Allocation.Height := AParams.Height;
    gtk_widget_size_allocate(NewWidget, @Allocation);
   //  gtk_widget_grab_default (NewWidget);
    if csDesigning in AWinControl.ComponentState then begin
      // at designtime setup normal handlers
      TGtk2WidgetSet(WidgetSet).FinishCreateHandle(AWinControl,NewWidget,AParams);
    end else begin
      // at runtime


   {    g_signal_connect(GPointer(NewWidget), 'destroy',
                       TGTKSignalFunc(@XTermWidgetDestroyCB), WidgetInfo);
     }

    end;

 //  g_signal_connect(GPointer(NewWidget), 'focus', g_callback(@uclicked),NIL);



    Result:=HWND({%H-}PtrUInt(Pointer(NewWidget)));

    DebugLn(['TWSEmbedder.CreateHandle ',dbgs(NewWidget)]);
    end;
end;

class procedure TWSEmbedder.DestroyHandle(const AWinControl: TWinControl
  );
begin
  inherited DestroyHandle(AWinControl);
end;
{$endif}

initialization
  {$ifdef Linux}
  RegisterWSComponent(TCustomEmbedder,TWSEmbedder);
  {$endif}

 // {$I xtermctrl.lrs}
end.
